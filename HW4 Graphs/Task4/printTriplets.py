import numpy as np


#Load the matrix
graph = np.loadtxt(open("smallWorldExample.csv"), delimiter=",", skiprows=0)

#Get its dimensions
N = np.shape(graph)

#The algorithm starts here
triplets = 0        #Initially there are zero triplets
triangles = 0       #Same for triangles

for i in range(N[0]):    #Search the columns     
    for j in range(N[1]):   #Search the rows
        if (i != j) and (graph[i][j] == 1):    #Find pairs
            for k in range(N[1]):   #One more loop for the triplets!
                    if (j != k) and (i != k) and (graph[j][k] == 1):  #Find triplets
                        triplets += 1
                        if (graph[i][k] == 1):  #Check if the triplet is a triangle 
                            triangles += 1
                            
triplets /= 6       #Each trplet was counted 6 times, so we divide by 6
triangles /= 6      #Same for triangles

#The results are...
print('Number of triangles:', triangles, ', triplets: ', triplets)
clustering = round(triangles/triplets,6)
print('Clustering coefficient: ', clustering)            

#THE ANSWER IS....
#Number of triangles: 1008.0 , triplets:  1649.0
#Clustering coefficient:  0.61128