import numpy as np


#Load the matrix
graph = np.loadtxt(open("smallWorldExample.csv"), delimiter=",", skiprows=0)

#Get its dimensions
N = np.shape(graph)

#Create an empty matrix with these dimensions to store the results
#distance = np.zeros(N)


f= open("pairs.txt","w+")

#The algorithm
for i in range(N[0]):               #Search the columns
    print('Writing ', i)                       #Reset the distance counter
    for j in range(N[1]):           #Search the rows
        if (i != j):                #Do not count the node itself
            if graph[i][j] == 1:
                print((i+1), (j+1), file = f)
                            
f.close()
print('Done!')            





#Save the matrix
#np.savetxt('distances.csv', distance, delimiter=",", fmt='%d')
