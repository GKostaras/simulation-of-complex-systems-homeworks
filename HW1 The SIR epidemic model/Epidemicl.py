#Giannis Kostaras, the SIR model simulation

import matplotlib.pyplot as plt
import numpy as np
import random

stepChance = 0.8 #probability of moving for each individual
N = 1000 #total population
nSick = 10 #initial number of sick individuals
spread_chance = 360000 #probability of epidemic to spread per-million
recover_chance = 3000 #chance to recover from disease per-million


#Tags representing the health of each individual
HEALTHY = 0
SICK = 1
RECOVERED = 2


#The individual prototype
class Individual():
    def __init__(self, count, x, y, health):
        self.x = x
        self.y = y
        self.health = health
                
    #Plot the individuals on the grid
    def plotIndividual(self):
        plt.xlim(-1, 101)
        plt.ylim(-1,101)
        if self.health == HEALTHY:
            plt.scatter(self.x, self.y, c="g", s=14)
        if self.health == SICK:
            plt.scatter(self.x, self.y, c="r", s=14)
        if self.health == RECOVERED:
            plt.scatter(self.x, self.y, c="b", s=14)

    

#Move the individual around with random steps   
def stepIndividual(Individual):
    stepProbability = random.randint(0,101)
    chooseStep = random.randint(1, 5)
    if stepProbability < stepChance * 100:
        
        if chooseStep == 1 and Individual.x != 0:
            Individual.x -= 1
        if chooseStep == 1 and Individual.x == 0:
            Individual.x += 1
            
        if chooseStep == 2 and Individual.x != 100:
            Individual.x += 1
        if chooseStep == 2 and Individual.x == 100:
            Individual.x -=1
        
        if chooseStep == 3 and Individual.y != 0:
            Individual.y -= 1
        if chooseStep == 3 and Individual.y == 0:
            Individual.y += 1
        
        if chooseStep == 4 and Individual.y != 100:
            Individual.y += 1
        if chooseStep == 4 and Individual.y == 100:
            Individual.y -= 1


#Create a test individual for debugging purposes
#testSubject = Individual(-1, x, y, 1)

#Create a grid where the epidemic sites will be designated as 1
grid_array = np.zeros( (101, 101) )

#Create a population of N individuals on a 100x100 grid
population = [] #Empty list of population
h = SICK
for i in range(1, N+1):
    if i > nSick:
        h = HEALTHY
    randx = random.randint(0,100)
    randy = random.randint(0,100)
    pop = Individual(i, randx, randy, h)
    if h == SICK:
        grid_array[randx][randy] = 1
    population.append(pop)


#Test if there are sick individuals on each site in this function
#sickpop = nSick
recoveredpop = 0
def check_epidemic():
    grid_array = np.zeros( (101, 101) )
    for i in range(0,N):
        global sickpop
        if population[i].health==SICK:
            #print(population[i].x,population[i].y,population[i].health)
            grid_array[population[i].x][population[i].y]=1 #designate epidemic site
    for j in range(0,N): #Time to spread the disease!
        Xcheck = population[j].x
        Ycheck = population[j].y
        if grid_array[Xcheck][Ycheck]==1 and population[j].health==HEALTHY:
            spreading = random.randint(0,1000000)
            if spreading<spread_chance:
                population[j].health=SICK
            else:
                population[j].health==HEALTHY

    for k in range(0,N):
        if population[k].health==SICK:
            check=random.randint(0,1000000)
            #print(k)
            if check<recover_chance:
                population[k].health=RECOVERED
                #print("TRUE", check)
            else:
                population[k].health=SICK
        #if population[j].health==SICK and random.randint(0,1000)<20:
           # population[j].health=RECOVERED






#TEST A FEW FRAMES..
plt.gcf().clear()




file1 = open("testfile.txt","w") 
    
for j in range(0,100000):
    countersick=0
    counterrec=0
    check_epidemic()
    for i in range(0, N):
        if population[i].health==SICK:
            countersick=countersick+1
        if population[i].health==RECOVERED:
            counterrec=counterrec+1
        stepIndividual(population[i])
    print(j,countersick, counterrec, N-(countersick+counterrec), file=file1)
    print(j,countersick, counterrec, N-(countersick+counterrec))
    if j<1000 and j%20==0:
        for m in range(0, N):
            population[m].plotIndividual()
        plt.savefig("%d.jpg" % j)
        plt.close()
        plt.gcf().clear()
    if j>499 and j%500==0:
        for m in range(0, N):
            population[m].plotIndividual()
        plt.savefig("%d.jpg" % j)
        plt.close()
        plt.gcf().clear()
    if countersick==0:
        file1.close()
        break




for i in range(0, N):
    population[i].plotIndividual()
plt.savefig("END.jpg")
plt.close()
plt.gcf().clear()