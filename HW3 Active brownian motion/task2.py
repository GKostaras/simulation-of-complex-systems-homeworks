import numpy as np
import matplotlib.pyplot as plt
from math import *
from pylab import plot, show, grid, axis, xlabel, ylabel, title
from random import randint



T = 120		#Timesteps
P = 4			#Population
V = .8			#Velocity
DT = .01		#Translational diffusion
DR = 0.08		#Rotational diffusion
t0 = 0        #Initialization of the animation
critical = 12    #Critical distance for torque effect
torqfactor = 3    #Torque strength

#Coordinate initialization
x = [T]
y = [T]
w = [T]


#The particle prototype
class Particle:
    def __init__(self, tag, x, y, w):
        self.tag = tag
        self.x = list(x)
        self.y = list(y)
        self.w = list(w)

    def plotParticle(self, t, color):
        plt.xlim(-100, 100)
        plt.ylim(-100, 100)
        plt.scatter(self.x[t], self.y[t], s=1, c=color)


#Empty list, prototype of the population
Population = []

#Create a population at t=0
for i in range(0, P):
    #Initialization
    p = Particle(i, x, y, w)
    #Values at t=0
    p.x[0] = 6*i - P/2
    p.y[0] = np.random.normal(0, 1)
    p.w[0] = (pi/2) * np.random.normal(0, 1)
    #make it a part of the population
    Population.append(p)



#Coordinate calculation for each moment t
for t in range(1, T):
    
    #Cycle through all particles at every moment
    for i in range(0, P):
        
        #The angle of particle i
        Population[i].w.append(Population[i].w[t-1] + sqrt(2*DR)*np.random.normal(0, 1))
        wi = Population[i].w[t-1]
        
        #Check the distances from particle i to the others
        for d in range(0, P):
            dist = sqrt( (Population[d].x[t-1] - Population[i].x[t-1])**2 + (Population[d].y[t-1] - Population[i].y[t-1])**2 )
            
            #Check the angle between particle i and particle d
            wd = Population[d].w[t-1]
            theta = wi - wd
            #print('angle between ', i, ' and ', d, ' = ', theta)
            
            if (dist <= critical) and (i != d):
                torq = torqfactor * (sin(theta)*cos(theta))
                Population[i].w[t] = (wi + torq)

        #Assign Coordinates for particle i at time t
        Population[i].x.append(Population[i].x[t-1] + V*cos(Population[i].w[t]) + sqrt(2*DT)*np.random.normal(0, 1))
        Population[i].y.append(Population[i].y[t-1] + V*sin(Population[i].w[t]) + sqrt(2*DT)*np.random.normal(0, 1))







#Plot the population
for i in range(0, P):
    print("Plotting particle ", i)
	#clr = np.random.rand(3,1)
    for j in range(t0, T):
        Population[i].plotParticle(j, i)

plt.show()










#____*** FOR DEBUG PURPOSES ***____
#test=Particle(1, {1, 2}, {1, 3})
#test.x = list(test.x)
#print(test.x[0])
#Population = []
#lst = {0, 1, 2}
#instance = Particle(2, lst, lst)
#Population.append(instance)
#print(instance.x[2])



