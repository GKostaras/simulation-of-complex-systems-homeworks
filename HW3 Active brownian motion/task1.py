import numpy as np
from math import *
from pylab import plot, show, grid, axis, xlabel, ylabel, title

T = 1000			#Timesteps
V = .5				#Velocity
DT = .1				#Translational diffusion
DR = .1			#Rotational diffusion

MSD = [T]
Wx = [T]
Wy = [T]
Ww = [T]
w = [T]
x = [T]
y = [T]


x[0]=0
y[0]=0
w[0]=pi/2



MSD[0] = 0



for t in range(1, T):
  w.append(w[t-1] + sqrt(2*DR)*np.random.normal(0, 1))
  x.append(x[t-1] + V*cos(w[t]) + sqrt(2*DT)*np.random.normal(0, 1))
  y.append(y[t-1] + V*sin(w[t]) + sqrt(2*DT)*np.random.normal(0, 1))
  
  msdx = (x[t]-x[t-1])*(x[t]-x[t-1]) / 2
  msdy = (y[t]-y[t-1])*(y[t]-y[t-1]) / 2
  MSD.append(MSD[t-1] + msdx + msdy)
  
  print(t, x[t], x[t-1], MSD[t])




file1 = open("output%sdiff.txt" %V,"w")
for el in range(0, T):
    file1.write(str(x[el])+','+str(y[el])+'\n')
file1.close()

file2 = open("msd%sdiff.txt" %V,"w")
for el in range(0, T):
    file2.write(str(el)+','+str(MSD[el])+'\n')
file2.close()
