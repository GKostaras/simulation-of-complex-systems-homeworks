# -*- coding: utf-8 -*-
"""
Created on Tue Nov 20 16:07:59 2018

@author: Giannis
"""

import numpy as np
import matplotlib.pyplot as plt
import random

 

#Parameters
N = 128       #Grid dimensions
p = 0.1      #Tree growth probability
f = 0.001      #Lightning strike probability
iterations=100
r=10

#Definitions
EMPTY=0
TREE=1
FIRE=2

class Site:
    def __init__(self, x, y, content):
        self.x=x
        self.y=y
        self.content=content


grid=np.zeros((N, N))

def populate(d):
    for x in range(0,N):
        for y in range(0,N):
            choice=random.random()
            if choice<=d:
                grid[x][y]=1

def lightning():
    for x in range(0,N):
        for y in range(0,N):
            choice=random.random()
            if choice<=f:
                grid[x][y]=2
                break
                
                

def countfires():
    fires=0
    for x in range(0,N):
        for y in range(0,N):
            if grid[x][y]==2:
                fires=fires+1
    return fires;

def counttrees():
    trees=0
    for x in range(0,N):
        for y in range(0,N):
            if grid[x][y]==1:
                trees=trees+1
    return trees; 

def spreadfires():
    for x in range(0,N):
        for y in range(0,N):
            if grid[x][y]==2:   #Check if there is a fire here
                if x!=0:
                    if grid[x-1][y]==1:
                        grid[x-1][y]=2
                if x!=N-1:
                    if grid[x+1][y]==1:
                        grid[x+1][y]=2
                if y!=0:
                    if grid[x][y-1]==1:
                        grid[x][y-1]=2
                if y!=N-1:
                    if grid[x][y+1]==1:
                        grid[x][y+1]=2
                if x==0:                    #Periodic boundary conditions
                    if grid[N-1][y]==1:
                        grid[N-1][y]=2
                if x==N-1:
                    if grid[0][y]==1:
                        grid[0][y]=2
                if y==0:
                    if grid[x][N-1]==1:
                        grid[x][N-1]=2
                if y==N-1:
                    if grid[x][0]==1:
                        grid[x][0]==2



def clearfires():
    for x in range(0,N):
        for y in range(0,N):
            if grid[x][y]==2:
                grid[x][y]=0
                
                  
def gridshow(name):
    plt.clf()
    for x in range(0,N):
        for y in range(0,N):
            if grid[x][y]==1:
                plt.scatter(x,y, c="g", s=4)
            if grid[x][y]==2:
                plt.scatter(x,y, c="r", s=4)
    plt.savefig("%d.jpg" %name, dpi=120)
    plt.clf()
    plt.close()


def maxfires():
       for i in range(0,10000):
        previous=countfires()
        spreadfires()
        if countfires()==previous:
            result=countfires()
            return result
            break 


fires1=[]
fires2=[]
f1=open("forest1.txt","a")
f2=open("forest2.txt","a")
print("Iteration, forest1 size, fire size: \n")
for i in range(0, iterations):
    populate(p)
    dens1=(counttrees())/(N*N)
    lightning()
    maxfires()
    fireno=countfires()/(N*N)
    clearfires()
    fires1.append(fireno)
    print(i, dens1, fireno)
    #print(fireno,file=f1)
    

grid=np.zeros((N, N))
print("\n\nIteration, forest2 size, fire size: \n")
for i in range(0,iterations):
    populate(dens1)
    lightning()
    maxfires()
    fireno=countfires()/(N*N)
    clearfires()
    fires2.append(fireno)
    print(i, dens1, fireno)
    #print(fireno,file=f2)
    grid=np.zeros((N, N))
    
def normalize(firelist):
    maxfire=max(firelist)
    minfire=min(firelist)
    normalized=[a/maxfire for a in(firelist)]
    return normalized;


plt.yscale('log')

data=fires1
# evaluate the histogram
values, base = np.histogram(data, bins=40)
#evaluate the cumulative
cumulative = np.cumsum(values)
# plot the cumulative function
print(len(data)-cumulative, file=f1)
plt.plot(base[:-1], len(data)-cumulative, c='blue')
#plot the survival function
#plt.plot(base[:-1], len(data)-cumulative, c='green')
data=fires2
# evaluate the histogram
values, base = np.histogram(data, bins=40)
#evaluate the cumulative
cumulative = np.cumsum(values)
# plot the cumulative function
print(len(data)-cumulative, file=f2)
plt.plot(base[:-1], len(data)-cumulative, c='green')
plt.show()
    
#print(normalize(fires1), file=f1)
#print(normalize(fires2), file=f2)




    
f1.close()
f2.close()